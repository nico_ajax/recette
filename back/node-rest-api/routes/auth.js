var express = require("express");
var router = express.Router();

var mongoose = require("mongoose");
var Users = require("../model/User");

const passport = require("passport");

const func = require("../controller/authController");

const auth = require("../middleware/auth_req");

//POST new user route (optional, everyone has access)
router.post("/signup", auth.optional, func.signUp);

//POST login route (optional, everyone has access)
router.post("/login", auth.optional, func.logIn);

//GET current route (required, only authenticated users have access)
router.get("/current", auth.required, (req, res, next) => {
  const {
    payload: { id }
  } = req;

  return Users.findById(id).then(user => {
    if (!user) {
      return res.sendStatus(400);
    }

    return res.json({ user: user.toAuthJSON() });
  });
});

module.exports = router;
