let mongoose = require("mongoose");

let RecipeSchema = new mongoose.Schema({
  name: String,
  difficulty: String,
  country: String,
  duration: String,
  type: String,
  ingredients: Array,
  explication: String,
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("recette", RecipeSchema);
