let mongoose = require("mongoose");
let Recipe = require("../model/Recipe.js");

function getRecipes(req, res, next) {
  Recipe.find({}, null, { sort: { name: 1 } }, function(err, products) {
    if (err) return next(err);
    res.json(products);
  });
}

function getRecipe(req, res, next) {
  Recipe.findById(req.params.id, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

function addRecipe(req, res, next) {
  Recipe.create(req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

function searchRecipes(req, res, next) {
  let category = req.params.category;
  let content = req.params.content;
  let query = {};
  query[category] = new RegExp(content, "i");
  console.log(query);
  Recipe.find(query, null, { sort: { name: 1 } }, function(err, products) {
    if (err) return next(err);
    res.json(products);
  });
}

function modifyRecipe(req, res, next) {
  Recipe.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

function deleteRecipe(req, res, next) {
  Recipe.findByIdAndRemove(req.params.id, req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

module.exports = {
  getRecipes,
  getRecipe,
  addRecipe,
  searchRecipes,
  modifyRecipe,
  deleteRecipe
};
