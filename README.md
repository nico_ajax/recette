Recettes du Q pour Bootcamp
===========

Une application simple pour explorer la programmation web dans le cadre du bootcamp. 
On produit donc une version très très downgraded de marmiton : un front lié à un back lié à une base de donnée MongoDB. On peut consulter la liste de recherche, faire une recherche, ajouter des recettes, modifier ou supprimer une recette.

Lancement de l'application avec Docker (recommandé)
===========

root : $ docker-compose up --build


Lancement de l'application sans Docker
===========

ouvrir trois consoles :
- la première pour exécuter la commande $ mongod
- la deuxième pour exécuter la commande $ cd livre-recette && ng serve --open
- la troisième pour exécuter la commande $ cd back/node-rest-api && npm install && npm start







COPYRIGHT
========

©Ajax