import { Ingredient } from './ingredient';

export class Recipe {
    _id: string ;
    name: string;
    difficulty: string;
    country: string;
    duration: string;
    type: string;
    ingredients: Ingredient[];
    explication: string;
}
