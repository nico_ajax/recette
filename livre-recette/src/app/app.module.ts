import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';

import { AppRoutingModule } from './/app-routing.module';

import { ListeRecetteService } from './services/liste-recette.service/liste-recette.service';
import { ChatService } from './services/chat.service/chat.service';
import { AuthService } from './services/auth.service/auth.service';
import { CitationService } from './services/citation.service/citation.service';

import { CitationDirective } from './directives/citation.directive';

import { AuthGuard } from './services/auth.service/auth.guard';
import { RequestOptions } from '@angular/http';
import { AuthRequestOptions } from './services/auth.service/auth-request';


import { AppComponent } from './app.component';
import { AddComponent } from '../app/components/add/add.component';
import { BannerComponent } from './components/banner/banner.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatDisplayComponent } from './components/chat-display/chat-display.component';
import { CitationBannerComponent } from './components/citation-banner/citation-banner.component';
import { CitationComponent } from './components/citation/citation.component';
import { DisplayComponent } from './components/display/display.component';
import { LoginComponent } from './components/login/login.component';
import { LoginformComponent } from './components/loginform/loginform.component';
import { ModifyComponent } from './components/modify/modify.component';
import { RecetteDetailComponent } from './components/recette-detail/recette-detail.component';
import { SearchComponent } from './components/search/search.component';
import { SignupformComponent } from './components/signupform/signupform.component';




@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    DisplayComponent,
    AddComponent,
    RecetteDetailComponent,
    ModifyComponent,
    ChatComponent,
    LoginComponent,
    LoginformComponent,
    SignupformComponent,
    BannerComponent,
    ChatDisplayComponent,
    CitationDirective,
    CitationBannerComponent,
    CitationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    NoopAnimationsModule,
    MatSidenavModule,
    ScrollDispatchModule
  ],
  providers: [
    ListeRecetteService,
    ChatService,
    AuthService,
    { provide: RequestOptions, useValue: AuthRequestOptions },
    AuthGuard,
    CitationService
  ],
  entryComponents: [CitationComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
