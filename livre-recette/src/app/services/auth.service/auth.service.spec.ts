import { TestBed, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { AuthService } from './auth.service';

let httpClientSpy: { get: jasmine.Spy };

describe('AuthService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'delete', 'post']);
    TestBed.configureTestingModule({
      providers: [AuthService, { provide: HttpClient, useValue: httpClientSpy }]
    });
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));
});
