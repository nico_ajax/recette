import { Injectable } from '@angular/core';
import { CitationItem } from '../../models/citation-item';
import { CitationComponent } from '../../components/citation/citation.component';

@Injectable()
export class CitationService {

  citations: Array<String>;

  constructor() {
    this.citations = [
      // tslint:disable-next-line:quotemark
      "Manger c'est bien",
      // tslint:disable-next-line:quotemark
      "Le ravioli crevette c'est le démon",
      // tslint:disable-next-line:quotemark
      "Vous êtes des cons et je suis votre roi",
      // tslint:disable-next-line:quotemark
      "On vous enverra de nouveaux projets"
    ];
  }

  sendRandomCitation(): String {
    const index = Math.floor(Math.random() * (this.citations.length - 1));
    return (this.citations[index]);
  }

  getAds() {
    return [
      // tslint:disable-next-line:quotemark
      new CitationItem(CitationComponent, "Manger c'est bien"),

      // tslint:disable-next-line:quotemark
      new CitationItem(CitationComponent, "Le ravioli crevette c'est le démon"),

      // tslint:disable-next-line:quotemark
      new CitationItem(CitationComponent, "Vous êtes des cons et je suis votre roi"),

      // tslint:disable-next-line:quotemark
      new CitationItem(CitationComponent, "On vous enverra de nouveaux projets"),
    ];
  }

}
