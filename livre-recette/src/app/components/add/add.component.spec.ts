import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddComponent } from './add.component';

import { FormsModule } from '@angular/forms';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Router } from '@angular/router';
import { RouterLinkDirectiveStub } from '../../testing/router-link-directive-stub';

let listeRecetteServiceStub: Partial<ListeRecetteService>;

listeRecetteServiceStub = {
  recipe: {
    _id: 'test', name: 'test', difficulty: '★', country: 'test', duration: 'tes',
    type: 'Quotidien', ingredients: [{ name: 'test', quantity: 'tes' }], explication: ''
  }
};

const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);


describe('AddComponent', () => {
  let component: AddComponent;
  let fixture: ComponentFixture<AddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [AddComponent],
      providers: [{ provide: ListeRecetteService, usevalue: listeRecetteServiceStub },
      { provide: Router, usevalue: routerSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
