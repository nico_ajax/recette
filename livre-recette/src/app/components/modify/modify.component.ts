import { Component, OnInit } from '@angular/core';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Router } from '@angular/router';
import { Recipe } from '../../models/recipe';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.css']
})
export class ModifyComponent implements OnInit {

  model = new Recipe();
  difficulties = ['★', '★★', '★★★', '★★★★', '★★★★★'];
  types = ['Quotidien', 'Exotique'];

  constructor(private listeRecetteService: ListeRecetteService,
    private router: Router) { }

  ngOnInit() {
    this.getRecipe();
  }

  addIngredient(newIngredientName: string, newIngredientQuantity: string) {
    if (newIngredientName && newIngredientQuantity) {
      this.model.ingredients.push({ name: newIngredientName, quantity: newIngredientQuantity });
    }
  }

  deleteIngredient(name: string): void {
    this.model.ingredients.splice(this.model.ingredients.indexOf(this.model.ingredients.find(model => model.name === name), 1));
  }

  getRecipe(): void {
    this.listeRecetteService.sendRecette()
      .subscribe(recette => this.model = recette);
    console.log(this.model.ingredients);
  }

  onSubmit() {
    this.listeRecetteService.modifyRecette(this.model).subscribe(
      _ => this.router.navigate(['/display/' + this.model._id])
    );
  }

}
