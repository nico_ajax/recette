import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyComponent } from './modify.component';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Router } from '@angular/router';
import { RouterLinkDirectiveStub } from '../../testing/router-link-directive-stub';

import { FormsModule } from '@angular/forms';


let listeRecetteServiceStub: Partial<ListeRecetteService>;

listeRecetteServiceStub = {
  recipe: {
    _id: 'test', name: 'test', difficulty: '★', country: 'test', duration: 'tes',
    type: 'Quotidien', ingredients: [{ name: 'test', quantity: 'tes' }], explication: ''
  }
};

const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl', 'events']);

describe('ModifyComponent', () => {
  let component: ModifyComponent;
  let fixture: ComponentFixture<ModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ModifyComponent, RouterLinkDirectiveStub],
      providers: [{ provide: ListeRecetteService, useValue: listeRecetteServiceStub }, { provide: Router, useValue: routerSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyComponent);
    component = fixture.componentInstance;
    /// fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
