import { Component, Input } from '@angular/core';
import { CitationInterfaceComponent } from '../../models/citation-interface.component';

@Component({
  selector: 'app-citation',
  templateUrl: './citation.component.html',
  styleUrls: ['./citation.component.css']
})
export class CitationComponent implements CitationInterfaceComponent {
  @Input() citation: String;

  constructor() { }



}
