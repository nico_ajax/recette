import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth.service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signupform',
  templateUrl: './signupform.component.html',
  styleUrls: ['./signupform.component.css']
})
export class SignupformComponent implements OnInit {

  model = new User();

  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  signUp(confirm: string): void {
    if (this.model.password && this.model.email && confirm) {
      console.log(this.model.password);
      console.log(confirm);
      if (this.model.password === confirm) {
        this.authService.signup(this.model).subscribe((obj) => {
          this.authService.setToken(obj.user.token);
          console.log(obj.user.token);
          console.log('signed in ' + obj.user.email);
          this.router.navigate(['/display']);
        });
      } else {
        alert('il faut rentrer le même mot de passe');
      }
    } else {
      if (!this.model.email) {
        alert('You must enter an email!');
      } else if (!this.model.password) {
        alert('You must enter an password!');
      } else if (!confirm) {
        alert('Merci de confirmer votre mot de passe');
      }
    }
  }
}
