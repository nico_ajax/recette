import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { LoginformComponent } from './loginform.component';
import { AuthService } from '../../services/auth.service/auth.service';
import { Router } from '@angular/router';

let AuthServiceStub: Partial<AuthService>;

AuthServiceStub = {};

const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

describe('LoginformComponent', () => {
  let component: LoginformComponent;
  let fixture: ComponentFixture<LoginformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginformComponent],
      imports: [FormsModule],
      providers: [{ provide: AuthService, useValue: AuthServiceStub },
      { provide: Router, usevalue: routerSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
