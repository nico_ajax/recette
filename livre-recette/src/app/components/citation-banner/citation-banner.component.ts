import { Component, OnInit, ComponentFactoryResolver, Input, ViewChild, OnDestroy } from '@angular/core';
import { CitationDirective } from '../../directives/citation.directive';
import { CitationItem } from '../../models/citation-item';
import { CitationInterfaceComponent } from '../../models/citation-interface.component';

@Component({
  selector: 'app-citation-banner',
  templateUrl: './citation-banner.component.html',
  styleUrls: ['./citation-banner.component.css']
})
export class CitationBannerComponent implements OnInit, OnDestroy {
  @Input() citations: CitationItem[];
  currentAdIndex = -1;
  @ViewChild(CitationDirective) citationHost: CitationDirective;
  interval: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.loadComponent();
    this.getCitations();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  loadComponent() {
    this.currentAdIndex = (this.currentAdIndex + 1) % this.citations.length;
    const citationItem = this.citations[this.currentAdIndex];

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(citationItem.component);

    const viewContainerRef = this.citationHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<CitationInterfaceComponent>componentRef.instance).citation = citationItem.citation;
  }

  getCitations() {
    this.interval = setInterval(() => {
      this.loadComponent();
    }, 3000);
  }
}
