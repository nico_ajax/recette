import { Component, OnInit } from '@angular/core';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Search } from '../../models/search';
import { CitationService } from '../../services/citation.service/citation.service';
import { CitationItem } from '../../models/citation-item';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  request = new Search();

  categories = ['name', 'country', 'type'];

  citations: CitationItem[];

  constructor(private listeRecetteService: ListeRecetteService,
    private citationService: CitationService) { }

  ngOnInit() {
    this.citations = this.citationService.getAds();
  }

  addSearch(searchName: string, searchType: string) {
    if (searchName && searchType) {
      this.request.name = searchName,
        this.request.type = searchType;
      this.listeRecetteService.getSearchParam(this.request);
    } else {
      alert('Recherche invalide');
    }
  }

}
