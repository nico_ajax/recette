import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchComponent } from './components/search/search.component';
import { DisplayComponent } from './components/display/display.component';
import { AddComponent } from './components/add/add.component';
import { RecetteDetailComponent } from './components/recette-detail/recette-detail.component';
import { ModifyComponent } from './components/modify/modify.component';
import { LoginComponent } from './components/login/login.component';

import { AuthGuard } from './services/auth.service/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: '/display', pathMatch: 'full' },
  { path: 'display', component: DisplayComponent },
  { path: 'search', component: SearchComponent },
  { path: 'add', component: AddComponent, canActivate: [AuthGuard] },
  { path: 'display/:id', component: RecetteDetailComponent },
  { path: 'modify', component: ModifyComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
